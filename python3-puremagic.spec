Name:           python3-puremagic
Version:        1.28
Release:        1%{?dist}

Summary:        The automated Usenet download tool
License:        MIT
URL:            https://github.com/cdgriffith/puremagic

BuildArch:      noarch

Source0: https://github.com/cdgriffith/puremagic/archive/refs/tags/%{version}.tar.gz

BuildRequires: python3-build
BuildRequires: python3-devel
BuildRequires: python3-installer
BuildRequires: python3-rpm-macros
BuildRequires: python3-setuptools
BuildRequires: python3-wheel


%description
Pure python implementation of identifying files based off their magic numbers


%prep
%setup -q -n puremagic-%{version}


%build
%py3_build


%install
%py3_install


%files
%license LICENSE
%doc CHANGELOG.md README.rst
%{python3_sitelib}/puremagic*


%changelog
%autochangelog
